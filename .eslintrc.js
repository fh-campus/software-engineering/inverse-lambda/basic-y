module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
    node: true,
  },
  extends: 'standard',
  globals: {
    __static: true,
  },
  plugins: [
    'html'
  ],
  'rules': {

    'arrow-parens': 0,                                            // allow paren-less arrow functions
    'generator-star-spacing': 0,                                  // allow async-await
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0, // allow debugger during development

    'no-useless-return': 'warn', // useful during development (no error, just warn)
    'no-useless-escape': 'off',  // conflicts with some XRegExp escapes
    'no-control-regex':  'off',  // necessary when filtering
    'no-restricted-syntax': 'off', // allow array destructing

    // 'no-mixed-operators': "off", // responsibility to the developer

    // Avoid curly braces for single statements (personal preference)
    'curly': ['off', 'multi-or-nest'], // multi-or-nest could be nice, but makes development tedious

    'space-infix-ops': ['off', /*{ 'int32Hint': true }*/], // don't require spaces around infix operators
    'key-spacing': ['error', { // no space after colons in single line listings (personal preference)
      'singleLine': { 'beforeColon':false, 'afterColon':false },
      'multiLine': {
          'beforeColon' :false,
          'afterColon': true,
          'align':      'value'
      }
    }],

    'standard/no-callback-literal': 'off',

    // // Require dangling commas on multiline (barely a pp, this is a must, sorry IE8)
    // 'comma-dangle': ['error', {
    //   'arrays':    'always', // allow array destructing, with optional parameters (always = only option which allows trailing comma on the same line)
    //   'objects':   'always-multiline',
    //   'imports':   'always-multiline',
    //   'exports':   'always-multiline',
    //   'functions': 'always-multiline',
    // }],
    'comma-dangle': 'off',
  }
}
