import Vue from 'vue'
import axios from 'axios'
// import { ipcRenderer } from 'electron' //

import App from './App'
import router from './router'
import store from './store'
import VueTabsChrome from 'vue-tabs-chrome'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.use(VueTabsChrome)

export const messageBus = new Vue({}) //

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template:   '<App/>',
}).$mount('#app')

// ipcRenderer.on('file', (event, fn, stat) => {
//   messageBus.$emit('file', fn, stat)
// })

// ipcRenderer.on('directory', (event, fn, stat) => {
//   messageBus.$emit('directory', fn, stat)
// })
