a: # adheres to (base / unchanged parameters)
  # 'https://inverse.lambda/std@0.1.0/http/field.y'
  # field: 'https://inverse.lambda/std@0.1.0/http/field.y'
  # click: 'https://inverse.lambda/std@0.1.0/handler/clickable.y'
  field: './field.y'

b: # additional parameters (and their types)
  label: text          # @nullable oä annotations erlaubt?
  click: 'event<text>' # Ein event dass text zurück gibt
  # x: field.x
  # y: field.y
  # width: field.width
  # height: field.height

c: [] # (sub) component (interfaces) # <- this is an implementation attribute!!!

p: # parameters and their types
  'passed on click value': 'possibly not here'
  label: text # @nullable oä annotations erlaubt?